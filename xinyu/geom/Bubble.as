﻿package xinyu.geom{
	import xinyu.collection.HashMap;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	public class Bubble extends Sprite{
		
		private var _radius:Number;
		private var _color:int;
		private var _borderCol:int;
		private var _txtCol:int;
		private var _thickness:Number;
		private var _textField:TextField;
		private var _txtFmt:TextFormat;
		private var _dots:Array;
		private var _actDot:String;
		public static const NUM_DOTS:int = 36;
		
		public var hashMap:HashMap;
				
		public function Bubble(thickness:Number = 6, borderCol:int = 0x000000, txtCol:int = 0x000000, color:int = 0xCCCCCC, radius:Number = 50){			
			hashMap = new HashMap();
			
			_color = color;
			_txtCol = txtCol;
			_borderCol = borderCol;
			_thickness = thickness;
			_radius = radius;			
			
			this.buttonMode = true;
			this.useHandCursor = true;
			
			this.graphics.beginFill(color);
			this.graphics.lineStyle(_thickness, _borderCol, 1);
			this.graphics.drawCircle(0,0,radius);
			this.graphics.endFill();

			_dots = new Array(NUM_DOTS);
			for(var i:int = 0; i<_dots.length;i++){
				_dots[i] = new Dot(0x00ff00, 6);
				addChild(_dots[i]);
				_dots[i].name = i.toString();
				_dots[i].alpha = 0;
				_dots[i].x = (_radius+3)*Math.cos(Math.PI*2/_dots.length*i);
				_dots[i].y = (_radius+3)*Math.sin(Math.PI*2/_dots.length*i);
				_dots[i].addEventListener(MouseEvent.MOUSE_OVER, onDotOver, false, 0, true);
				_dots[i].addEventListener(MouseEvent.MOUSE_OUT, onDotOut, false, 0, true);
				_dots[i].addEventListener(MouseEvent.CLICK, onDotDown, false, 0, true);
			}
			
			_txtFmt = new TextFormat();
			_txtFmt.bold =  true;
			_txtFmt.size = 14;
			_txtFmt.align = TextFormatAlign.CENTER;
			_txtFmt.font = "Verdana";
			
			_textField = new TextField();
			addChild(_textField);
			_textField.type = TextFieldType.INPUT;
			_textField.border = false;
			_textField.borderColor = 0xffffff;
			_textField.autoSize = TextFieldAutoSize.CENTER;
			_textField.width = _radius*Math.cos(Math.asin(18/_radius))*2;
			_textField.height = 18;
			_textField.x = -_textField.width/2;
			_textField.y = -_textField.height/2;
			_textField.wordWrap = false;
			_textField.multiline = true;
			_textField.textColor = txtCol;
			_textField.embedFonts = true;
			
			_textField.defaultTextFormat = _txtFmt;
			_textField.addEventListener(TextEvent.TEXT_INPUT, onTextInput, false, 0, true);
			_textField.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver, false, 0, true);
			_textField.addEventListener(FocusEvent.FOCUS_OUT, onMouseOut, false, 0, true);
			_textField.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut, false, 0, true);
		}
		
		private function onMouseOver(event:Event):void{
			event.target.border = true;
		}
		
		private function onMouseOut(event:Event):void{
			event.target.border = false;
			this.dispatchEvent(new Event("onTextFocusOut"));
			event.target.dispatchEvent(new TextEvent(TextEvent.TEXT_INPUT));
		}
		
		private function onTextInput(event:Event):void{
			_textField.x = -_textField.width/2;
			_textField.y = -_textField.height/2;
			repaint(_thickness, _borderCol, _txtCol, _color, (event.target.width/2)/(Math.cos(Math.atan(event.target.height/event.target.width)))+3);
			this.dispatchEvent(new Event("onTextLineChanged"));
		}
		
		public function repaint(thickness:Number = 6, borderCol:int = 0x000000, txtCol:int = 0x000000, color:int = 0xCCCCCC, radius:Number = 50):void{
			_color = color;
			_txtCol = txtCol;
			_borderCol = borderCol;
			_thickness = thickness;
			_radius = radius;
			
			if(radius>= 10){
				_radius = radius
				this.graphics.clear();
				this.graphics.beginFill(_color);
				this.graphics.lineStyle(_thickness, _borderCol, 1);
				this.graphics.drawCircle(0,0,_radius);
				this.graphics.endFill();
				
				//_textField.width = _radius*Math.cos(Math.PI/4)*2;
				//_textField.height = _radius*Math.sin(Math.PI/4)*2;
				_textField.x = -_textField.width/2;
				_textField.y = -_textField.height/2;
				_textField.textColor = _txtCol;
				
				for(var i:int = 0; i<_dots.length;i++){
					_dots[i].x = (_radius+3)*Math.cos(Math.PI*2/_dots.length*i);
					_dots[i].y = (_radius+3)*Math.sin(Math.PI*2/_dots.length*i);
				}
			}
		}
		
		public function get radius():Number{
			return _radius;
		}
		
		private function onDotOver(event:MouseEvent):void{
			event.target.alpha = 1;
		}
		
		private function onDotOut(event:MouseEvent):void{
			event.target.alpha = 0;
		}
		
		private function onDotDown(event:MouseEvent):void{
			_actDot = event.target.name;
			event.target.parent.dispatchEvent(new Event("onDotActivated"));
		}
		
		public function get actDot():String{
			return _actDot;
		}
		
		public function getDotPos(i:int):Position2D{
			return new Position2D(this.x + (_radius+2)*Math.cos(Math.PI*2/_dots.length*i), this.y + (_radius+2)*Math.sin(Math.PI*2/_dots.length*i))
		}
		
		public function get text():String{
			return _textField.text;
		}
		
		public function set text(txt:String):void{
			_textField.text = txt;
		}
		
		public function get thickness():Number{
			return _thickness;
		}
		
		public function get borderCol():int{
			return _borderCol;
		}
		
		public function get txtCol():int{
			return _txtCol;
		}
		
		public function get color():int{
			return _color;
		}
	}
}
