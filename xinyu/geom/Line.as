﻿package xinyu.geom{
	import flash.display.Sprite;
	
	import xinyu.science.Science;
	
	public class Line extends Sprite{
		private var _thickness:Number;
		private var _color:Number;
		private var _alpha:Number;
		private var _length:Number;
		private var _radian:Number;
		private var _x1:Number;
		private var _y1:Number;
		private var _x2:Number;
		private var _y2:Number;
		
		public var startObj:String;
		public var endObj:String;
		
		public function Line(thickness:Number = 1, color:int = 0x000000, alpha:Number = 1, x1:Number = 0, y1:Number = 0, x2:Number = 0, y2:Number = 0){
			_thickness = thickness;
			_color = color;
			_alpha = alpha;
			_length = length;
			_x1=x1;
			_y1=y1;
			_x2=x2;
			_y2=y2;
			
			this.graphics.lineStyle(_thickness, _color, _alpha);
			this.graphics.moveTo(_x1,_y1);
			this.graphics.lineTo(_x2,_y2);
		}
		
		public function setLength(length:Number, radian:Number):void{
			_length = length;
			_radian = radian;
			
			_x2 = Math.cos(_radian)*_length;
			_y2 = Math.sin(_radian)*_length;			
			
			this.graphics.clear();
			this.graphics.lineStyle(_thickness, _color, _alpha);
			this.graphics.moveTo(0,0);
			this.graphics.lineTo(_x2, _y2);			
		}
		
		public function repaint(x1:Number ,y1:Number, x2:Number, y2:Number):void{
			_x1=x1;
			_y1=y1;
			_x2=x2;
			_y2=y2;
			this.graphics.clear();
			this.graphics.lineStyle(_thickness, _color, _alpha);
			this.graphics.moveTo(_x1, _y1);
			this.graphics.lineTo(_x2, _y2);
		}
		
		public function getLength():Number{
			return Science.distance(_x1, _y1, _x2, _y2);
		}
		
		public function getRadian():Number{
			return Math.atan2(_y2-_y1, _x2-_x1);
		}
	}
}