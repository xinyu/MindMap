﻿package xinyu.geom{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	
	import xinyu.geom.ItemShape;
	
	public class BubbleItem extends Sprite implements ItemShape{
		
		private var _width:Number;
		private var _height:Number;
		private var _color:int;
		private var _borderCol:int;
		private var _thickness:Number;
				
		public function BubbleItem(thickness:Number = 6, borderCol:int = 0x000000, color:int = 0xCCCCCC, width:Number = 50, height:Number = 50){
			_color = color;
			_borderCol = borderCol;
			_thickness = thickness;
			_width = width;
			_height = height;
			
			this.buttonMode = true;
			this.useHandCursor = true;
			
			this.graphics.beginFill(color);
			this.graphics.lineStyle(_thickness, _borderCol, 1);
			this.graphics.drawCircle(0,0,width/2);
			this.graphics.endFill();
		}
		
		public function repaint(thickness:Number, borderCol:int, bodyColor:int, width:Number, height:Number):void{
			_color = bodyColor;
			_borderCol = borderCol;
			_thickness = thickness;
			_width = width;
			_height = height;
			
			this.graphics.clear();
			this.graphics.beginFill(_color);
			this.graphics.lineStyle(_thickness, _borderCol, 1);
			this.graphics.drawCircle(0,0,width/2);
			this.graphics.endFill();
		}
		
		public function getWidth():Number{
			return _width;
		}
		
		public function getHeight():Number{
			return _height;
		}
	}
}