﻿package xinyu.geom{
	import xinyu.geom.Line;
	import xinyu.geom.Bubble;
	
	public class ConnRecord{
		public var startObj:String;
		public var endObj:String;
		public var line:Line;
		
		public function ConnRecord(startObj:String = null, endObj:String = null, line:Line = null){
			this.startObj = startObj;
			this.endObj = endObj;
			this.line = line;
		}
	}
}