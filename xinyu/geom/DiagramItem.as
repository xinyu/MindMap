﻿package xinyu.geom{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	
	import xinyu.geom.ItemShape;
	import xinyu.geom.BubbleItem;
	import xinyu.geom.RectItem;	
	import xinyu.collection.HashMap;
	
	public class DiagramItem extends Sprite{
		private var _shape:ItemShape;
		private var _bubble:BubbleItem;
		private var _rect:RectItem;
		private var _rectBorder:Sprite;
		private var _bubbleBorder:Sprite;
		
		private var _sID:String;
		public var _bodyColor:int;
		public var _borderCol:int;
		public var _txtCol:int;
		public var _thickness:Number;
		public var _width:Number;
		public var _height:Number;
		
		private var _textField:TextField;
		private var _txtFmt:TextFormat;
		
		public var hashMap:HashMap;
		
		public function DiagramItem(thickness:Number = 6, borderCol:int = 0x000000, txtCol:int = 0x000000, bodyColor:int = 0xCCCCCC, width:Number = 100, height:Number = 75, sID:String = "Rectangle"){
			var i:int;
			hashMap = new HashMap();
			
			_bodyColor = bodyColor;
			_borderCol = borderCol;
			_txtCol = txtCol;
			_thickness = thickness;
			_width = width;
			_height = height;
			_sID = sID;
			
			_rect = new RectItem(thickness, borderCol, bodyColor, width, height);
			_bubble = new BubbleItem(thickness, borderCol, bodyColor, width, height);
			_rect.visible = false;
			_bubble.visible = false;
			_rect.doubleClickEnabled = true;
			_rect.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClicked);
			_bubble.doubleClickEnabled = true;
			_bubble.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClicked);
			
			
			_rectBorder = new Sprite();
			_rectBorder.graphics.clear();
			_rectBorder.graphics.lineStyle(thickness+2, 0x00ff00,1);
			_rectBorder.graphics.drawRect(-_rect.getWidth()/2, -_rect.getHeight()/2, _rect.getWidth(), _rect.getHeight());
			_rectBorder.addEventListener(MouseEvent.MOUSE_OVER, onReactiveOver, false, 0, true);
			_rectBorder.addEventListener(MouseEvent.MOUSE_OUT, onReactiveOut, false, 0, true);
			_rectBorder.addEventListener(MouseEvent.CLICK, onReactiveClicked, false, 0, true);
			_rectBorder.visible = false;
			_rectBorder.alpha = 0;
			_rectBorder.mouseEnabled = true;
			
			_bubbleBorder = new Sprite();
			_bubbleBorder.graphics.clear();
			_bubbleBorder.graphics.lineStyle(thickness+2, 0x00ff00,1);
			_bubbleBorder.graphics.drawCircle(0,0,_bubble.getWidth()/2);
			_bubbleBorder.addEventListener(MouseEvent.MOUSE_OVER, onReactiveOver, false, 0, true);
			_bubbleBorder.addEventListener(MouseEvent.MOUSE_OUT, onReactiveOut, false, 0, true);
			_bubbleBorder.addEventListener(MouseEvent.CLICK, onReactiveClicked, false, 0, true);
			_bubbleBorder.visible = false;
			_bubbleBorder.alpha = 0;
			_bubbleBorder.mouseEnabled = true;
			
			addChild(_rect);
			addChild(_rectBorder);
			addChild(_bubble);
			addChild(_bubbleBorder);
						
			switch(_sID){
				case "Bubble":
					_bubble.visible = true;
					_bubbleBorder.visible = true;
					_shape = _bubble;
					break;
				case "Rectangle":
					_rect.visible = true;
					_rectBorder.visible = true;
					_shape =  _rect;
					break;
			}
			
			_txtFmt = new TextFormat();
			_txtFmt.bold =  true;
			_txtFmt.size = 14;
			_txtFmt.align = TextFormatAlign.CENTER;
			_txtFmt.font = "Verdana";
			
			_textField = new TextField();
			addChild(_textField);
			_textField.type = TextFieldType.INPUT;
			_textField.border = false;
			_textField.borderColor = 0xffffff;
			_textField.autoSize = TextFieldAutoSize.CENTER;
			
			switch(_sID){
				case "Bubble":
					_textField.width = width;
					break;
				case "Rectangle":
					_textField.width = _width/2*Math.cos(Math.asin(18/_width/2))*2;
					break;
			}
			
			_textField.height = 18;
			_textField.x = -_textField.width/2;
			_textField.y = -_textField.height/2;
			_textField.wordWrap = false;
			_textField.multiline = true;
			_textField.textColor = txtCol;
			_textField.embedFonts = true;
			
			_textField.defaultTextFormat = _txtFmt;
			
			_textField.addEventListener(TextEvent.TEXT_INPUT, onTextInput, false, 0, true);
			_textField.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver, false, 0, true);
			_textField.addEventListener(FocusEvent.FOCUS_OUT, onMouseOut, false, 0, true);
			_textField.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut, false, 0, true);
		}
		
		private function onTextInput(event:Event):void{
			_textField.x = -_textField.width/2;
			_textField.y = -_textField.height/2;
			switch(_sID){
				case "Bubble":
					var radius2:Number = (event.target.width)/(Math.cos(Math.atan(event.target.height/event.target.width)))+3;
					_width = _height = radius2;
					repaint(_thickness, _borderCol, _txtCol, _bodyColor, _width, _height);
					
					_bubbleBorder.graphics.clear();
					_bubbleBorder.graphics.lineStyle(thickness+2, 0x00ff00,1);
					_bubbleBorder.graphics.drawCircle(0,0,_width/2);
					
					break;
				case "Rectangle":
					_width = event.target.width+6;
					_height = event.target.height+20;
					repaint(_thickness, _borderCol, _txtCol, _bodyColor, _width, _height);
					
					_rectBorder.graphics.clear();
					_rectBorder.graphics.lineStyle(thickness+2, 0x00ff00,1);
					_rectBorder.graphics.drawRect(-_width/2, -_height/2, _width, _height);
					
					break;
			}
			this.dispatchEvent(new Event("onTextLineChanged"));
		}
		
		public function repaint(thickness:Number, borderCol:int, txtCol:int, bodyColor:int, width:Number, height:Number):void{
			_thickness = thickness;
			_borderCol = borderCol;
			_txtCol = txtCol;
			_bodyColor = bodyColor;
			_width = width;
			_height = height;
			
			_textField.textColor = _txtCol;
			_shape.repaint(_thickness, _borderCol, bodyColor, width, height);
		}
		
		private function onDoubleClicked(event:MouseEvent):void{
			switch(_sID){
				case "Bubble":
					_sID = "Rectangle";
					_rect.visible = true;
					_rectBorder.visible = true;
					_bubble.visible = false;
					_bubbleBorder.visible = false;
					
					_shape = _rect;
					repaint(_thickness, _borderCol, _txtCol, _bodyColor, event.target.width+6, event.target.height+20);
					
					break;
				case "Rectangle":
					_sID = "Bubble";
					_rect.visible = false;
					_rectBorder.visible = false;
					_bubble.visible = true;
					_bubbleBorder.visible = true;
					
					_shape = _bubble;
					var radius:Number = (event.target.width/2)/(Math.cos(Math.atan(event.target.height/event.target.width)))+3;
					repaint(_thickness, _borderCol, _txtCol, _bodyColor, radius, radius);
					break;
			}
			_textField.dispatchEvent(new TextEvent(TextEvent.TEXT_INPUT));
		}
		
		
		private function onMouseOver(event:Event):void{
			event.target.border = true;
		}
		
		private function onMouseOut(event:Event):void{
			event.target.border = false;
			this.dispatchEvent(new Event("onTextFocusOut"));
			event.target.dispatchEvent(new TextEvent(TextEvent.TEXT_INPUT));
		}
		
		private function onReactiveOver(event:MouseEvent):void{
			event.target.alpha = 1;
		}
		
		private function onReactiveOut(event:MouseEvent):void{
			event.target.alpha = 0;
		}
		
		private function onReactiveClicked(event:MouseEvent):void{
			event.target.parent.dispatchEvent(new Event("onReactiveClicked"));
		}
		
		public function get getWidth():Number{
			return _width;
		}
		
		public function get getHeight():Number{
			return _height;
		}
		
		public function get text():String{
			return _textField.text;
		}
		
		public function set text(txt:String):void{
			_textField.text = txt;
		}
		
		public function get thickness():Number{
			return _thickness;
		}
		
		public function get borderCol():int{
			return _borderCol;
		}
		
		public function get txtCol():int{
			return _txtCol;
		}
		
		public function get color():int{
			return _bodyColor;
		}
		
		public function get type():String{
			return _sID;
		}
		
		public function destory():void{
			_rectBorder.removeEventListener(MouseEvent.MOUSE_OVER, onReactiveOver);
			_rectBorder.removeEventListener(MouseEvent.MOUSE_OUT, onReactiveOut);
			_rectBorder.removeEventListener(MouseEvent.CLICK, onReactiveClicked);
			
			_bubbleBorder.removeEventListener(MouseEvent.MOUSE_OVER, onReactiveOver);
			_bubbleBorder.removeEventListener(MouseEvent.MOUSE_OUT, onReactiveOut);
			_bubbleBorder.removeEventListener(MouseEvent.CLICK, onReactiveClicked);
			
			_textField.removeEventListener(TextEvent.TEXT_INPUT, onTextInput);
			_textField.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			_textField.removeEventListener(FocusEvent.FOCUS_OUT, onMouseOut);
			_textField.removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
	}
}