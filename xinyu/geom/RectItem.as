﻿package xinyu.geom{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	
	import xinyu.geom.ItemShape;
	
	public class RectItem extends Sprite implements ItemShape{
		
		private var _length:Number;
		private var _color:int;
		private var _borderCol:int;
		private var _thickness:Number;
		private var _width:Number;
		private var _height:Number;
				
		public function RectItem(thickness:Number = 6, borderCol:int = 0x000000, color:int = 0xCCCCCC, width:Number = 100, height:Number = 75){
			_color = color;
			_borderCol = borderCol;
			_thickness = thickness;
			_width = width;
			_height = height;
			
			this.buttonMode = true;
			this.useHandCursor = true;
			
			this.graphics.beginFill(color);
			this.graphics.lineStyle(_thickness, _borderCol, 1);
			this.graphics.drawRect(-_width/2, -_height/2, _width, _height);
			this.graphics.endFill();
		}
		
		public function repaint(thickness:Number, borderCol:int,bodyColor:int, width:Number, height:Number):void{
			_color = bodyColor;
			_borderCol = borderCol;
			_thickness = thickness;
			_width = width;
			_height = height;
			
			this.graphics.clear();
			this.graphics.beginFill(_color);
			this.graphics.lineStyle(_thickness, _borderCol, 1);
			this.graphics.drawRect(-_width/2, -_height/2, _width, _height);
			this.graphics.endFill();
		}
		
		public function getWidth():Number{
			return _width;
		}
		
		public function getHeight():Number{
			return _height;
		}
	}
}