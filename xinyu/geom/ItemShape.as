﻿package xinyu.geom{
	public interface ItemShape{
		function repaint(thickness:Number, borderCol:int, bodyColor:int, width:Number, height:Number):void;
		function getWidth():Number;
		function getHeight():Number;
	}
}