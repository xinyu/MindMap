﻿package xinyu.geom{
	import flash.display.Sprite;
	
	public class Dot extends Sprite{
		private var _radius:Number;
		private var _color:Number;
		
		public function Dot(color:int = 0x000000, radius:Number = 6){
			_radius = radius;
			_color = color;
			refresh(_color, _radius);
		}
	
		public function refresh(color:int, radius:Number):void{
			_color = color;
			_radius = radius;
			this.graphics.beginFill(color);
			this.graphics.drawCircle(0,0,radius);
			this.graphics.endFill();
		}
	}	
}