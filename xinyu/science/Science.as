﻿/* Science */
package xinyu.science{
	public class Science {
		public function Science() {
		}
		public static function radToDeg(rad:Number):Number {
			return rad / Math.PI * 180;
		}
		public static function degToRad(deg:Number):Number {
			return deg / 180 * Math.PI;
		}
		public static function randInt(from:int = 0, end:int = 100):int {
			if (from > end) {
				var temp:int = end;
				end = from;
				from = temp;
			}
			return Math.round(Math.random() * end - from) - Math.abs(from);
		}
		public static function findMax(...args):* {
			var max:* = args[0];

			for (var i:int = 1; i <args.length; i++) {
				if (max < args[i]) {
					max = args[i];
				}
			}
			return max;
		}
		public static function findMin(...args):* {
			var min:* = args[0];

			for (var i:int = 1; i <args.length; i++) {
				if (min > args[i]) {
					min = args[i];
				}
			}
			return min;
		}
		public static function distance(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			return Math.sqrt(Math.pow(x2 - x1,2) + Math.pow(y2 - y1,2));
		}
		public static function angleBetween(x1:Number, y1:Number, x2:Number, y2:Number):Number {
			return Math.atan2(y2 - y1,x2 - x1);
		}
		
		public static function fastSine(x:Number):Number {
			var sin:Number = 0;
			if (x < -3.14159265) {
				x += 6.28318531;
			} else if (x >  3.14159265) {
				x -= 6.28318531;

			}
			
			if (x < 0) {
				sin = 1.27323954 * x + .405284735 * x * x;
			} else {
				sin = 1.27323954 * x - 0.405284735 * x * x;
			}
			return sin;
		}

		public static function fastCosine(x:Number):Number {
			var cos:Number = 0;
			x += 1.57079632;
			if (x >  3.14159265) {
				x -= 6.28318531;
			}

			if (x < 0) {
				cos = 1.27323954 * x + 0.405284735 * x * x;
			} else {
				cos = 1.27323954 * x - 0.405284735 * x * x;
			}
			return cos;
		}

		public static function fastSine2(x:Number):Number {
			var sin:Number = 0;
			//always wrap input angle to -PI..PI
			if (x < -3.14159265) {
				x += 6.28318531;
			} else if (x >  3.14159265) {
				x -= 6.28318531;
			}
			//compute sine
			if (x < 0) {
				sin = 1.27323954 * x + .405284735 * x * x;

				if (sin < 0) {
					sin = .225 * (sin *-sin - sin) + sin;
				} else {
					sin = .225 * (sin * sin - sin) + sin;
				}
			} else {
				sin = 1.27323954 * x - 0.405284735 * x * x;

				if (sin < 0) {
					sin = .225 * (sin *-sin - sin) + sin;
				} else {
					sin = .225 * (sin * sin - sin) + sin;
				}
			}
			return sin;
		}

		public static function fastCosine2(x:Number):Number {
			var cos:Number = 0;
			//compute cosine: sin(x + PI/2) = cos(x)
			x += 1.57079632;
			if (x >  3.14159265) {
				x -= 6.28318531;
			}

			if (x < 0) {
				cos = 1.27323954 * x + 0.405284735 * x * x;

				if (cos < 0) {
					cos = .225 * (cos *-cos - cos) + cos;
				} else {
					cos = .225 * (cos * cos - cos) + cos;
				}
			} else {
				cos = 1.27323954 * x - 0.405284735 * x * x;

				if (cos < 0) {
					cos = .225 * (cos *-cos - cos) + cos;
				} else {
					cos = .225 * (cos * cos - cos) + cos;
				}
			}
			return cos;
		}
	}
}