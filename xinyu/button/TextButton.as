﻿package xinyu.button{
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.motion.Color;

	public class TextButton extends Sprite {

		private var _w:Number;
		private var _h:Number;
		private var _rad:Number;
		private var _linW:Number;
		private var _col:uint;
		private var _txt:String;
		private var _txtCol:uint;

		public function TextButton(w:Number, h:Number, rad:Number, linW:Number, col:uint, txt:String, txtCol:uint) {
			_w = w;
			_h = h;
			_rad = rad;
			_linW = linW;
			_col = col;
			_txt = txt;
			_txtCol = txtCol;
			this.name = _txt;
			
			var btn:SimpleButton = createBtn();
			addChild(btn);
			var labl:TextField = createLabel();
			labl.mouseEnabled = false;
			addChild(labl);
		}
		
		private function createBtn() {
 			var btn:SimpleButton = new SimpleButton();
			btn.upState = createRoundRect(_col);
			var ovCol:uint = Color.interpolateColor(_col, 0xFFFFFF, .3)
			btn.overState = createRoundRect(ovCol);
			var dnCol:uint = Color.interpolateColor(_col, 0x000000, .3)
			btn.downState = createRoundRect(dnCol);
			btn.hitTestState = btn.upState;
			return btn;
		}
		
		private function createRoundRect(col:uint):Shape {
			var rRect:Shape = new Shape();
			rRect.graphics.beginFill(col, 0.5);
			rRect.graphics.lineStyle(_linW, _col, 1.0);
			rRect.graphics.drawRoundRect(0, 0, _w, _h, _rad);
			rRect.graphics.endFill();
			return rRect;
		}
		
		private function createLabel():TextField {
			var txt:TextField = new TextField();
			txt.width = _w;
			txt.height = _h;

			var format:TextFormat = new TextFormat();
			format.font = "Verdana";
			format.color = _txtCol;
			format.size = 10;
			format.bold = true;
			format.align = TextFormatAlign.CENTER

			txt.defaultTextFormat = format;
			txt.text = _txt;
			txt.selectable = false;
			
			return txt;
		}
	}
}