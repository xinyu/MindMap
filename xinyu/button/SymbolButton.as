﻿package xinyu.button{
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import fl.motion.Color;

	public class SymbolButton extends Sprite {
		private var _size:Number;
		private var _rad:Number;
		private var _linW:Number;
		private var _btnCol:uint;
		private var _symOffset:Number;
		private var _sym:String;

		public function SymbolButton(size:Number, rad:Number, linW:Number, btnCol:uint, sym:String) {
			_size = size;
			_rad = rad;
			_linW = linW;
			_btnCol = btnCol;
			_symOffset = (Math.sqrt(size*size*2)-size)/2+1;
			_sym = sym;

			var btn:SimpleButton = createBtn();
			addChild(btn);
		}
				
		private function createBtn():SimpleButton {
 			var btn:SimpleButton = new SimpleButton();
			var ovCol:uint = Color.interpolateColor(_btnCol, 0xFFFFFF, .3);
			var dnCol:uint = Color.interpolateColor(_btnCol, 0x000000, .3);
			btn.upState = createRoundRect(_btnCol, dnCol);			
			btn.overState = createRoundRect(ovCol, dnCol);			
			btn.downState = createRoundRect(dnCol, ovCol);
			btn.hitTestState = btn.upState;
			return btn;
		}
		
		private function createRoundRect(bgCol:uint, symCol:uint):Shape {
			var rRect:Shape = new Shape();
			rRect.graphics.beginFill(bgCol, .5);
			rRect.graphics.lineStyle(_linW, _btnCol, 1);
			rRect.graphics.drawRoundRect(0, 0, _size, _size, _rad);
			createSym(rRect, symCol);
			rRect.graphics.endFill();
			return rRect;
		}
		
		private function createSym(rRect:Shape, symCol:uint):void{
			switch(_sym){
				case "close":
					rRect.graphics.lineStyle(_linW, symCol, 1);
					rRect.graphics.moveTo(_symOffset, _symOffset);
					rRect.graphics.lineTo(_size-_symOffset, _size-_symOffset);
					rRect.graphics.moveTo(_size-_symOffset, _symOffset);
					rRect.graphics.lineTo(_symOffset, _size-_symOffset);
					break;
				case "maximize":
					rRect.graphics.lineStyle(_linW, symCol, 1);
					rRect.graphics.moveTo(_symOffset, _symOffset);
					rRect.graphics.lineTo(_symOffset, _size-_symOffset);
					
					rRect.graphics.moveTo(_symOffset, _symOffset);
					rRect.graphics.lineTo(_size-_symOffset, _symOffset);
					
					rRect.graphics.moveTo(_size-_symOffset, _size-_symOffset);
					rRect.graphics.lineTo(_symOffset, _size-_symOffset);
					
					rRect.graphics.moveTo(_size-_symOffset, _size-_symOffset);
					rRect.graphics.lineTo(_size-_symOffset, _symOffset);
					break;
				case "minimize":
					rRect.graphics.lineStyle(_linW, symCol, 1);
					rRect.graphics.moveTo(_symOffset, _size-_symOffset);
					rRect.graphics.lineTo(_size-_symOffset, _size-_symOffset);
					break;
			}
		}
		
		/*** for test only **/
		public function getSymName():String{
			return _sym;
		}
	}
}