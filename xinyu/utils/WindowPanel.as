﻿/*===========================================================================
*
*	Project		:WindowPanel 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.04.30			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:WindowPanel by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.utils{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.filters.GradientBevelFilter;
	import flash.display.Shape;
	import fl.controls.TextArea;
	
	import xinyu.button.SymbolButton;
	
	public class WindowPanel extends Sprite{
		private var _closeBtn:SymbolButton;
		private var _title:TextField;
		//private var _msg:TextField;
		private var _msg:TextArea;
		private var _titleFmt:TextFormat;
		private var _msgFmt:TextFormat;
		private var _titleBar:Shape;
		private var _outShape:Shape;
		private var _innShape:Shape;
		
		public function WindowPanel(title:String = "", msg:String = "", width:Number = 320, height:Number = 240, alpha:Number = 0.85, color:int = 0x8DABCB){
			_outShape = new Shape();
			addChild(_outShape);
			_outShape.graphics.beginFill(color);
			_outShape.graphics.drawRoundRect(0, 0, width, height, 10);
			_outShape.graphics.endFill();
			_outShape.filters = [new DropShadowFilter()];
			_outShape.alpha = alpha;
			
			_innShape = new Shape();
			_innShape.graphics.beginFill(0xffffff);
			_innShape.graphics.drawRect(0,0,width-20, height-40);
			_innShape.graphics.endFill();
			_innShape.filters = [new GradientBevelFilter(2,45,[0xCCCCCC, 0xffffff, 0x666666], [1,1,1], [0,128,255], 2, 2, 1, 1)];
			
			this.addChild(_innShape);
			_innShape.x = 10;
			_innShape.y = 30;
			
			_titleBar = new Shape();
			_titleBar.graphics.beginFill(color);
			_titleBar.graphics.drawRect(0,0,width-20, 20);
			_titleBar.graphics.endFill();
			_titleBar.filters = [new GradientBevelFilter(1,45,[0xffffff, color], [0.5, 0.5], [0,255] , 1, 1, 1, 1)];
			
			this.addChild(_titleBar);
			_titleBar.x = 10;
			_titleBar.y = 5;			
			
			_titleFmt = new TextFormat();
			_titleFmt.bold =  true;
			_titleFmt.size = 12;
			_titleFmt.align = TextFormatAlign.LEFT;
			_titleFmt.font = "Verdana";
			
			_msgFmt = new TextFormat();
			_msgFmt.bold =  false;
			_msgFmt.size = 12;
			_msgFmt.align = TextFormatAlign.LEFT;
			_msgFmt.font = "Verdana";
			
			_title = new TextField();
			addChild(_title);
			_title.width = width - 24;
			_title.x = 12;
			_title.y = 5;
			_title.text = title;
			_title.textColor = 0xffffff;
			_title.selectable = false;
			_title.setTextFormat(_titleFmt);
			//_title.defaultTextFormat =  _titleFmt;
			_title.addEventListener(MouseEvent.MOUSE_DOWN, onDragStart);
			_title.addEventListener(MouseEvent.MOUSE_UP, onDragStop);
			
			/*
			_msg = new TextField();
			addChild(_msg);
			_msg.width = width - 20;
			_msg.height = _innShape.height; 
			_msg.x = 12;
			_msg.y = 32;
			_msg.textColor = 0x000000;
			_msg.wordWrap = true;
			_msg.text = msg;
			_msg.setTextFormat(_msgFmt);
			_msg.defaultTextFormat = _msgFmt;
			*/
			
			_msg = new TextArea();
			addChild(_msg);
			_msg.width = width - 20;
			_msg.height = _innShape.height; 
			_msg.x = 12;
			_msg.y = 32;
			_msg.htmlText = msg;
			_msg.editable = false;
			//_msg.setStyle("textFormat", _msgFmt);
			
			_closeBtn = new SymbolButton(20, 2, 2, color, "close");
			addChild(_closeBtn);
			_closeBtn.x = width-30;
			_closeBtn.y = 5;
			_closeBtn.addEventListener(MouseEvent.CLICK, onCloseClicked);
		}
		
		private function onDragStart(event:MouseEvent):void{
			event.target.parent.startDrag();
			
			if(this.parent.getChildIndex(this) != this.parent.numChildren-1){
				this.parent.swapChildren(this, this.parent.getChildAt(this.parent.numChildren-1));
			}
		}
		
		private function onDragStop(event:MouseEvent):void{
			event.target.parent.stopDrag();
		}
		
		private function onCloseClicked(event:MouseEvent):void{
			event.target.parent.parent.visible = false;
		}
	}
}