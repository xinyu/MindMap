﻿/*===========================================================================
*
*	Project		:AlertPanel
*	Author		:Lin Xin-Yu
*	Date		:2009.04.30			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:AlertPanel by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.utils{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	
	import xinyu.button.TextButton;
	
	public class AlertPanel extends Sprite{
		private var _okBtn:TextButton;
		private var _title:TextField;
		private var _msg:TextField;
		private var _titleFmt:TextFormat;
		private var _msgFmt:TextFormat;
		
		public function AlertPanel(msg:String = "", title:String = "", width:Number = 300, height:Number = 160, alpha:Number = 0.85, color:int = 0x8DABCB){
			this.graphics.beginFill(color);
			this.graphics.drawRoundRect(0, 0, width, height, 10);
			this.graphics.endFill();
			this.filters = [new DropShadowFilter()];
			this.alpha = alpha;
			this.visible = false;
			
			_titleFmt = new TextFormat();
			_titleFmt.bold =  true;
			_titleFmt.size = 12;
			_titleFmt.align = TextFormatAlign.LEFT;
			_titleFmt.font = "Verdana";
			
			_msgFmt = new TextFormat();
			_msgFmt.bold =  false;
			_msgFmt.size = 12;
			_msgFmt.align = TextFormatAlign.CENTER;
			_msgFmt.font = "Verdana";
			
			_title = new TextField();
			addChild(_title);
			_title.width = width - 10;
			_title.x = 5;
			_title.y = 5;
			_title.text = title;
			_title.textColor = 0xCC0000;
			_title.setTextFormat(_titleFmt);
			_title.selectable = false;
			_title.defaultTextFormat =  _titleFmt;
			
			_msg = new TextField();
			addChild(_msg);
			_msg.width = width - 20;
			_msg.x = 10;
			_msg.y = height/2 - 20;
			_msg.textColor = 0xffffff;
			_msg.wordWrap = true;
			_msg.htmlText = msg;
			_msg.setTextFormat(_msgFmt);
			_msg.autoSize = TextFieldAutoSize.CENTER;
			_msg.defaultTextFormat = _msgFmt;
			
			
			_okBtn = new TextButton(80,20,10,2,0xCCCCCC,"OK",0x000000);
			addChild(_okBtn);
			_okBtn.x = width/2 - 40;
			_okBtn.y = height/2+35;
			_okBtn.addEventListener(MouseEvent.CLICK, onOkBtnClicked);
		}
		
		private function onOkBtnClicked(event:MouseEvent):void{
			this.visible = false;;
			this.dispatchEvent(new Event("onAlertDisabled"));
		}
		
		public function show(msg:String = "", title:String = ""):void{
			_msg.text = msg;
			_title.text = title;
			this.visible = true;
			
			if(this.parent.getChildIndex(this) != this.parent.numChildren-1){
				this.parent.swapChildren(this, this.parent.getChildAt(this.parent.numChildren-1));
			}
			
			this.dispatchEvent(new Event("onAlertEnabled"));
		}
	}
}