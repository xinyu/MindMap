﻿/*===========================================================================
*
*	Project		:Map 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.04.22			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	Description	:This implementation based on the work of Michael Baczynski 
*				 was modified by Lin Xin-Yu. To get more detail information, 
*				 please visit following website.
*	
*				 Michael Baczynski, http://www.polygonal.de
*				 Lin Xin-Yu, 		http://nxforce.blogspot.com
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.collection{
	import flash.utils.Dictionary;
	
	import xinyu.collection.Map;
	import xinyu.collection.HashMapNode;
	import xinyu.collection.Iterator;
	import xinyu.collection.HashIterator;

	public class HashMap implements Map {
		private var head:HashMapNode = new HashMapNode();
		private var tail:HashMapNode = new HashMapNode();

		public function HashMap() {
			head.next = tail;
			tail.prev = head;
		}

		public function insert(key:*, obj:*):Boolean {
			if (key == null || obj == null)	return false;
			if (searchByKey(key) != null) return false;
			
			var node:HashMapNode = new HashMapNode(key, obj, tail.prev, tail);
			node.prev.next = node;
			tail.prev = node;
			
			return true;
		}
		
		public function find(key:*):*{
			var node:HashMapNode = searchByKey(key);
			if(node) return node.obj;
			return null;
		}
		
		public function remove(key:*):Boolean{
			var node:HashMapNode = searchByKey(key);
			if(node == null)	return false;
						
			node.prev.next = node.next;
			node.next.prev = node.prev;
			return true;
		}
		
		public function keySet():Array{
			var array:Array = new Array();
			var curr:HashMapNode = head.next;
			
			while(curr!=tail){
				array.push(curr.key);
				curr = curr.next;
			}
			
			return array;
		}
		
		public function entrySet():Array{
			var array:Array = new Array();
			var node:HashMapNode = head.next;
			var dict:Dictionary = new Dictionary(true);
			
			while(node != tail){
				if(dict[node.obj] == undefined){
					dict[node.obj] = 1;
					array.push(node.obj);
				}
				node = node.next;
			}
			return array;
		}

		public function clear():void{
			var curr:HashMapNode = head.next;
			var nextPtr:*;
			while(curr!=tail){
				curr.key = null;
				curr.obj = null;
				nextPtr = curr.next;
				curr.next = curr.prev = null;
				curr = nextPtr;				
			}
			
			head.next = tail;
			tail.prev = head;
		}
		
		public function swap(key1:*, key2:*):Boolean{
			if (key1 == null||key2 == null)	return false;
			
			var node1:HashMapNode = searchByKey(key1);
			var node2:HashMapNode = searchByKey(key2);
			if(node1 == null || node2 == null)	return false;
			
			var temp:HashMapNode = new HashMapNode();
			temp.key = node1.key;
			temp.obj = node1.obj;
			
			node1.key = node2.key;
			node1.obj = node2.obj;
			
			node2.key = temp.key;
			node2.obj = temp.obj;

			return true;			
		}
		
		public function isEmpty():Boolean {
			if (head.next == tail)	return true;
			return false;
		}
		
		public function moveUp(key:*):Boolean{
			if(key == null) return false;
			
			var node:HashMapNode = searchByKey(key);
			if(node == null) return false;
			if(node.prev == head) return false;
			return swap(key, node.prev.key);
		}
		
		public function moveDown(key:*):Boolean{
			if(key == null) return false;
			
			var node:HashMapNode = searchByKey(key);
			if(node == null) return false;
			if(node.next == tail) return false;
			
			return swap(key, node.next.key);
		}
		
		public function get size():int{
			var count:int = 0;
			var curr:HashMapNode = head.next;
			
			while(curr!=tail){
				count++;
				curr=curr.next;
			}
			
			return count;
		}
		
		public function containsKey(key:*):Boolean{
			if(searchByKey(key)) return true;
			return false;
		}
		
		public function containsValue(obj:*):Boolean{
			var curr:HashMapNode = new HashMapNode();
			curr = head.next;
			
			while(curr!=tail){
				if(curr.obj === obj) return true;
				curr = curr.next;
			}
			
			return false;
		}
		
		private function searchByKey(key:*):HashMapNode{
			var curr:HashMapNode = new HashMapNode();
			curr = head.next;
			
			while(curr!=tail){
				if(curr.key === key) return curr;
				curr = curr.next;
			}
			
			return null;
		}
		
		public function iterator():Iterator{
			return new HashIterator(head,tail);
		}
	}
}