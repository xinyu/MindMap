﻿/*===========================================================================
*
*	Project		:Node
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.03.11			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:MindMap by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/
package xinyu.collection{
	public class Node {
		public var next:Node = null;
		public var data:Object = null;
		
		public function Node(data:Object = null, next:Node = null){
			this.data = data;
			this.next = next;
		}
	}
}