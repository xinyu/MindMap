﻿/*===========================================================================
*
*	Project		:Stack 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.03.11			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:MindMap by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/
package xinyu.collection{
	import xinyu.collection.Node;
	public class Stack {
		private var _top:Node;
		private var _length:Number;
		
		public function Stack(){
			_top = null;
			_length = 0;
		}

		public function isEmpty():Boolean {
			return _top == null;
		}
		
		public function push(data : Object):void {
			var newTop:Node = new Node(data, _top);
			_top = newTop;
			_length += 1;
		}
		
		public function pop():Object {
			if (isEmpty ()) {
				return "empty";
			}
			
			_length -= 1;
			var data:Object = _top.data;
			_top = _top.next;
			return data;
		}
		
		public function peek():Object {
			if (isEmpty ()) {
				return "empty";
			}
			return _top.data;
		}
		
		public function get length():Number{
			return _length;
		}
		
		public function flush():void{
			_length = 0;
			_top = null;
		}
	}
}