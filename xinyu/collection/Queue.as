﻿/*===========================================================================
*
*	Project		:Queue 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.03.11			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:MindMap by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.collection{
	import xinyu.collection.Node;
	
	public class Queue {
		private var _bottom:Node;
		private var _top:Node;
		private var _length:Number;
		
		public function Queue(){
			_bottom = _top = null;
			_length = 0;
		}
		
		public function isEmpty():Boolean {
			return (_bottom == null);
		}
		
		public function enqueue(data:Object):void {
			var node:Node = new Node(data, null);
			if (isEmpty()) {
				_bottom = node;
				_top = node;
			} else {
				_top.next = node;
				_top = node;
			}
			_length += 1;
		}
		
		public function dequeue():Object{
			if (isEmpty()) {
				return "empty";
			}
			
			_length -= 1;
			var data:Object = _bottom.data;
			_bottom = _bottom.next;
			return data;
		}
		
		public function peek():Object {
			if (isEmpty()) {
				return "empty";
			}
			
			return _bottom.data;
		}
		
		public function get length():Number{
			return _length;
		}
	}
}