﻿/*===========================================================================
*
*	Project		:Map 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.04.22			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	Description	:This implementation based on the work of Michael Baczynski 
*				 was modified by Lin Xin-Yu. To get more detail information, 
*				 please visit following website.
*	
*				 Michael Baczynski, http://www.polygonal.de
*				 Lin Xin-Yu, 		http://nxforce.blogspot.com
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.collection{
	import xinyu.collection.Iterator;
	import xinyu.collection.HashMapNode;
	
	public class HashIterator implements Iterator{
		private var head:HashMapNode;
		private var tail:HashMapNode;
		private var curr:HashMapNode;
		
		public function HashIterator(head:HashMapNode, tail:HashMapNode){
			this.head = head;
			this.tail = tail;
			curr = head.next;
		}
				
		public function hasNext():Boolean{
			return (curr != tail);
		}
		
		public function next():*{
			if(curr != tail){
				var obj:* = curr.obj;
				curr = curr.next;
				return obj;
			}
			return null;
		}
		
		public function rewind():void{
			curr = head.next;
		}
		
		public function remove():void{
			tail.prev = tail.prev.prev;
			tail.prev.next = tail;
		}
	}
}