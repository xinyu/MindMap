﻿/*===========================================================================
*
*	Project		:Map 
*	Version		:1.0
*	Author		:Lin Xin-Yu
*	Date		:2009.04.22			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	Description	:This implementation based on the work of Michael Baczynski 
*				 was modified by Lin Xin-Yu. To get more detail information, 
*				 please visit following website.
*	
*				 Michael Baczynski, http://www.polygonal.de
*				 Lin Xin-Yu, 		http://nxforce.blogspot.com
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package xinyu.collection{
	public interface Map{
		function insert(key:*, obj:*):Boolean;
		function find(key:*):*;
		function remove(key:*):Boolean;
		function keySet():Array;
		function entrySet():Array;
		function clear():void;
		function isEmpty():Boolean;
		function iterator():Iterator;
		function containsValue(obj:*):Boolean;
		function containsKey(key:*):Boolean;
	}
}