﻿/*===========================================================================
*
*	Project		:MindMap
*	Version		:1.2
*	Author		:Lin Xin-Yu
*	Date		:2009.06.11			
*	E-mail		:xinyu0123@gmail.com
*	Blog		:http://xinyu.byethost3.com
*	
*	License		:MindMap by Lin Xin-Yu is licensed under a Creative Commons 
*				 Attribution-Noncommercial-Share Alike 3.0 Unported License
*	ChangeLog	:
*				 Version 1.0 -> First version.
*
*				 Version 1.1 -> Add Load/Save function
*							 -  Autofix printing orientation.
*
*				 Version 1.2 -> Add rectangular diagram item
*							 -  Add right click menu
*							 -  Add Move Up/Down function
*							 -  Change the code name to MindMap
*							 -  Compatible to Linux
*							 -  Performance Optimized
*							 -  Fix several bugs
*
*	#if you have any questions or suggestions, feel free to contact me!
*
*===========================================================================*/

package {
	/************************************************/
	/* import libraries								*/
	/************************************************/
	
	/* Import flash core library */
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.filters.*;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.KeyboardEvent;
	import flash.events.ContextMenuEvent;
	
	import flash.printing.PrintJob;
	import flash.printing.PrintJobOrientation;
	
	import flash.utils.Timer;
	import flash.utils.ByteArray;
	
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.desktop.ClipboardTransferMode;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.geom.Rectangle;
	import flash.net.FileReference;
	import flash.net.FileFilter;
	import flash.filters.DropShadowFilter;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import fl.controls.TextArea;
	import fl.controls.Button;
	
	/* import xinyu's library */
	import xinyu.geom.Line;
	import xinyu.geom.Position2D;
	import xinyu.science.Science;
	import xinyu.geom.Dot;
	import xinyu.utils.AlertPanel;
	import xinyu.utils.WindowPanel;
	import xinyu.button.TextButton;
	import xinyu.collection.HashMap;
	import xinyu.geom.DiagramItem;
	import xinyu.geom.ItemShape;
	import xinyu.collection.Iterator;
	import xinyu.collection.HashIterator;
	
	/* import adobe's library */
	import com.adobe.images.PNGEncoder;
		
	public class MindMap extends Sprite{
		/************************************************/
		/* Variables declaration						*/
		/************************************************/
		
		/* Sprites */
		private var canvas:Sprite;
		private var stageMask:Sprite;
		private var controlPanel:Sprite;
		private var cldUI:Sprite;
		
		/* Panels */
		private var alert:AlertPanel;
		private var helpWindow:WindowPanel;
		private var aboutWindow:WindowPanel;
		
		/* Line and DiagramItem */
		private var line:Line;
		
		/* Main HashMap */
		private var allObjs:HashMap;
		
		/* Control Buttons */
		
		private var newIdeaBtn:Button;
		private var rootBtn:Button;
		private var loadBtn:Button;
		private var saveBtn:Button;
		private var copyToClipBtn:Button;
		private var snapShotBtn:Button;
		private var deleteBtn:Button;
		private var printBtn:Button;
		private var helpBtn:Button;
		private var aboutBtn:Button;
		
		private var listNodes:TextArea;
		
		/* Object */
		private var lastFocusObject:Object;
		private var rootObject:DiagramItem;
		private var startObj:DiagramItem;
		private var endObj:DiagramItem;
		private var dragObj:DiagramItem;
		
		private var itemStartIndex:int = 1;
		
		private var isReactiveActivated:Boolean = false;
		
		/* Timers */
		private var lineTimer:Timer;
		private var diagramItemTimer:Timer;
		
		/* FileReference */
		private var loadFileRef:FileReference;
		
		/*** const for A4 printing ***/
		private const border:int = 40;
		private const A4L_WIDTH:int = 766;
		private const A4L_HEIGHT:int = 515;
		private const A4P_WIDTH:int = 515;
		private const A4P_HEIGHT:int = 766;
		
		/************************************************/
		/* ClusterDiagram Constructor					*/
		/************************************************/
		
		public function MindMap(){
			/* initialize user interface */
			initUI();
			
			/* create a main hashmap for recording every child of cldUI */
			allObjs = new HashMap();
			
			/* Timer for updating line length while creating the connection */
			lineTimer = new Timer(35);
			lineTimer.addEventListener(TimerEvent.TIMER, onLineTimer);
			
			/* Timer for updating the line connection while dragging the item */ 
			diagramItemTimer = new Timer(50);
			diagramItemTimer.addEventListener(TimerEvent.TIMER, onDiagramItemTimer);
			
			this.addEventListener(MouseEvent.MOUSE_UP, onDragStop, false, 0, true);
			
			/* for listening the shortcut keys */
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onStageKeyDown, false, 0, true);
		}
		
		/************************************************/
		/* Build the user interface						*/
		/************************************************/
		
		private function initUI():void{
			canvas = new Sprite();
			canvas.graphics.beginFill(0xffffff);
			canvas.graphics.lineStyle(2,0x000000,1);
			canvas.graphics.drawRect(0, 0, A4L_WIDTH+10, A4L_HEIGHT+10);
			canvas.graphics.endFill();
			addChild(canvas);
			canvas.x = 2;
			canvas.y = 2;
			canvas.filters = [new DropShadowFilter(2, 45, 0.5, 4, 4, 1, 1)];
			
			cldUI = new Sprite();
			cldUI.graphics.beginFill(0xffffff);
			cldUI.graphics.drawRect(0, 0, A4L_WIDTH, A4L_HEIGHT);
			cldUI.graphics.endFill();
			canvas.addChild(cldUI);
			cldUI.x = 5;
			cldUI.y = 5;
			
			controlPanel = new Sprite();
			controlPanel.graphics.beginFill(0xffffff);
			controlPanel.graphics.lineStyle(2,0x000000,1);
			controlPanel.graphics.drawRect(0, 0, 215, A4L_HEIGHT+10);
			controlPanel.graphics.endFill();
			addChild(controlPanel);
			controlPanel.filters = [new DropShadowFilter(2, 45, 0.5, 4, 4, 1, 1)];
			controlPanel.x = 782;
			controlPanel.y = 2;
			
			stageMask = new Sprite();
			addChild(stageMask);
			stageMask.graphics.beginFill(0xffffff);
			stageMask.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			stageMask.graphics.endFill();
			stageMask.alpha = 0.5;
			stageMask.visible = false;			
			
			alert = new AlertPanel();
			addChild(alert);
			alert.x = stage.stageWidth/2 - alert.width/2;
			alert.y = stage.stageHeight/2 - alert.height/2;
			alert.addEventListener("onAlertEnabled", onAlertEnabled, false, 0, true);
			alert.addEventListener("onAlertDisabled", onAlertDisabled, false, 0, true);
			
			var txtFmt:TextFormat = new TextFormat();
			txtFmt.bold = true;
			txtFmt.font = "Verdana";
			txtFmt.size = 10;
			
			newIdeaBtn = new Button();
			controlPanel.addChild(newIdeaBtn);
			newIdeaBtn.label = "New Idea";
			newIdeaBtn.setStyle("textFormat", txtFmt);
			newIdeaBtn.x = 5;
			newIdeaBtn.y = 5;
			newIdeaBtn.addEventListener(MouseEvent.CLICK, onIdeaBtnClicked, false, 0, true);
			
			rootBtn = new Button();
			controlPanel.addChild(rootBtn);
			rootBtn.label = "Mark Root";
			rootBtn.setStyle("textFormat", txtFmt);
			rootBtn.x = 110;
			rootBtn.y = 5;
			rootBtn.addEventListener(MouseEvent.CLICK, onRootBtnClicked, false, 0, true);
			
			loadBtn = new Button();
			controlPanel.addChild(loadBtn);
			loadBtn.label = "Open...";
			loadBtn.setStyle("textFormat", txtFmt);
			loadBtn.x = 5;
			loadBtn.y = 30;
			loadBtn.addEventListener(MouseEvent.CLICK, onLoadBtnClicked, false, 0, true);
			
			saveBtn = new Button();
			controlPanel.addChild(saveBtn);
			saveBtn.label = "Save...";
			saveBtn.setStyle("textFormat", txtFmt);
			saveBtn.x = 110;
			saveBtn.y = 30;
			saveBtn.addEventListener(MouseEvent.CLICK, onSaveBtnClicked, false, 0, true);
			
			copyToClipBtn = new Button();
			controlPanel.addChild(copyToClipBtn);
			copyToClipBtn.label = "Copy Outline";
			copyToClipBtn.setStyle("textFormat", txtFmt);
			copyToClipBtn.x = 5;
			copyToClipBtn.y = 55;
			copyToClipBtn.addEventListener(MouseEvent.CLICK, onClipBtnClicked, false, 0, true);
			
			snapShotBtn = new Button();
			controlPanel.addChild(snapShotBtn);
			snapShotBtn.label = "Export...";
			snapShotBtn.setStyle("textFormat", txtFmt);
			snapShotBtn.x = 110;
			snapShotBtn.y = 55;
			snapShotBtn.addEventListener(MouseEvent.CLICK, onSnapShotClicked, false, 0, true);
			
			printBtn = new Button();
			controlPanel.addChild(printBtn);
			printBtn.label = "Print";
			printBtn.setStyle("textFormat", txtFmt);
			printBtn.x = 5;
			printBtn.y = 80;
			printBtn.addEventListener(MouseEvent.CLICK, printOnePerPage, false, 0, true);
			
			deleteBtn = new Button();
			controlPanel.addChild(deleteBtn);
			deleteBtn.label = "Delete";
			deleteBtn.setStyle("textFormat", txtFmt);
			deleteBtn.x = 110;
			deleteBtn.y = 80;
			deleteBtn.addEventListener(MouseEvent.CLICK, onDelBtnClicked, false, 0, true);
			
			helpBtn = new Button();
			controlPanel.addChild(helpBtn);
			helpBtn.label = "Help";
			helpBtn.setStyle("textFormat", txtFmt);
			helpBtn.x = 5;
			helpBtn.y = 105;
			helpBtn.addEventListener(MouseEvent.CLICK, onHelpClicked, false, 0, true);
			
			aboutBtn = new Button();
			controlPanel.addChild(aboutBtn);
			aboutBtn .label = "About";
			aboutBtn .setStyle("textFormat", txtFmt);
			aboutBtn.x = 110;
			aboutBtn.y = 105;
			aboutBtn.addEventListener(MouseEvent.CLICK, onAboutClicked, false, 0, true);
			
			listNodes = new TextArea();
			controlPanel.addChild(listNodes);
			listNodes.wordWrap = false;
			listNodes.x = 5;
			listNodes.y = 130;
			listNodes.width = 205;
			listNodes.height = 390;
			listNodes.addEventListener(Event.CHANGE, onTextFocusOut, false, 0, true);
			
			var helpMsg:String = "<b>Quick Guideline for Mind Maps</b><br></br><br></br><b>A. Creating a Cluster Diagram</b><br>1. To Add a new Idea DiagramItem</br><br>	Left Click on the \"New Idea\" button</br><br>	Left Click in center of diagramItem to add text</br><br>2. To Change Position of Idea DiagramItem</br><br>	Move curser inside diagramItem (but not center)</br><br>	Left Click and drag to new location</br>  <br>3. To Connect Related Thoughts</br><br>	Move curser to edge of Idea DiagramItem</br><br>	When green border appears, Left Click</br><br>	Move Curser to edge of related Idea DiagramItem</br><br>	When green border appears, Left click</br><br>4.To remove a Connection or DiagramItem</br><br>	Position curser over the object</br><br>	Left Click on it</br><br>	Press \"Delete\" Key or click \"Delete\" button</br><br>5.To identify the root idea</br><br>	Left click on Idea DiagramItem</br><br>	Click \"Mark Root\" button<br>6. To move up/down the DiagremItem</br><br>	Left Click the diagramItem</br><br>	Right Click to activate the context menu</br><br>	Click move up/down menu item.</br><br /><br /><b>B. Document Management</b></br><br>1. To Print the Cluster Diagram</br><br>	Click the Print button (Prints on A4 paper)</br><br>2. To export the diagram to PNG</br><br>	Click \"Export\" button</br><br>	Accept default file name</br><br>3. To copy the Text</br><br>	Click the \"Copy Outline\" button</br><br>	Open Your Word Processor</br><br>	Click Paste in Word Processor</br><br>4. To Open a diagram</br><br>	Click the \"Open\" button</br><br>	Locate your file (.cld extension)</br><br>	Click \"Open\" in the window</br><br>5. To Save a diagram</br><br>	Click the \"Save\" button</br><br>	Accept Default File Name (.cld extension)</br><br>	Click \"Save\" in the window</br><br>	***Open, Save, Export require FlashPlayer 10***</BR><br></br><br><b>C. Accessing Help/About</b></br><br>1. To obtain help</br><br>	Left Click the Help Button</br><br>2. To get information about program</br><br>	Left Click the About Button</br><br></br><br><b>D. Error Messages</b></br><br>1. Illegal Connection</br><br>This idea is already connected to another idea.  If you wish to change its connection, first  delete the existing connections from this diagramItem.</br><br>2. Redundant Line</br><br>	There is already a line connecting the two diagramItems.</br><br>3. While the program works in Flash 9, Open, Save, Export require FlashPlayer 10.  </br>";
			helpWindow = new WindowPanel("Help", helpMsg, 320, 240, 0.75);
			addChild(helpWindow);
			helpWindow.x = canvas.width/2 - helpWindow.width/2;
			helpWindow.y = canvas.height/2 - helpWindow.height/2;
			helpWindow.visible = false;
			helpWindow.name = "Help";
			
			var aboutMsg:String = "<b><font color=\"#0000CC\">MindMap vs. 1.2 (2009.06.12)</font></b><br></br><b>Copyright (c) 2009 <font color=\"#0000CC\"><u> Lin Xin-Yu</u></font></b></br><br /><br><b>	Programmers:</b></br><br /><br>	<font color=\"#006633\"><i>Lin Xin-Yu</i></font></br><br>	E-mail: xinyu0123@gmail.com<br/>	Blog:<a href=\"http://xinyu.byethost3.com\"><font color=\"#0000ff\"> $=~/Code/gi;</font><br /></a><a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\"></a><br /><span xmlns:dc=\"http://purl.org/dc/elements/1.1/\" href=\"http://purl.org/dc/dcmitype/MovingImage\" property=\"dc:title\" rel=\"dc:type\">MindMap</span> by <a xmlns:cc=\"http://creativecommons.org/ns#\" href=\"http://xinyu.byethost3.com/\" property=\"cc:attributionName\" rel=\"cc:attributionURL\"><font color=\"#0000ff\">Lin Xin-Yu</font></a> is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\"><font color=\"#0000ff\">Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported License</font></a>";
			aboutWindow = new WindowPanel("About", aboutMsg, 320, 240);
			addChild(aboutWindow); 
			aboutWindow.x = canvas.width/2 - aboutWindow.width/2;
			aboutWindow.y = canvas.height/2 - aboutWindow.height/2;
			aboutWindow.visible = false;
			aboutWindow.name = "About Cluster";
			
			var cdMenu:ContextMenu = new ContextMenu();
			
			var newIdeaItem:ContextMenuItem = new ContextMenuItem( "New Idea" );
			var markRootItem:ContextMenuItem = new ContextMenuItem( "Mark Root" );
			var moveUpItem:ContextMenuItem = new ContextMenuItem( "Move UP	(UP)" );
			var moveDownItem:ContextMenuItem = new ContextMenuItem( "Move DOWN	(DOWN)" );
			var loadItem:ContextMenuItem = new ContextMenuItem( "Load	(F7)" );
			var saveItem:ContextMenuItem = new ContextMenuItem( "Save	(F8)" );
			var cancelItem:ContextMenuItem = new ContextMenuItem( "Cancel	(ESC)" );
			var deleteItem:ContextMenuItem = new ContextMenuItem( "Delete	(Delete)" );
			var helpItem:ContextMenuItem = new ContextMenuItem( "Help	(F1)" );
			var aboutItem:ContextMenuItem = new ContextMenuItem( "About Us	(F2)" );
			
			
			newIdeaItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			markRootItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			moveUpItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			moveDownItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			loadItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			saveItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			cancelItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			deleteItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			helpItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			aboutItem.addEventListener( ContextMenuEvent.MENU_ITEM_SELECT, onItemSelected, false, 0, true);
			
			cancelItem.separatorBefore = true;
			deleteItem.separatorBefore = true;
			helpItem.separatorBefore = true;

			cdMenu.hideBuiltInItems();
			cdMenu.customItems.push(newIdeaItem, markRootItem, moveUpItem, moveDownItem, loadItem, saveItem, cancelItem, deleteItem, helpItem,  aboutItem);

			canvas.contextMenu = cdMenu;
		}
		
		/************************************************/
		/* Event handler for alert panels			*/
		/************************************************/
		
		/* Mask ON */
		private function onAlertEnabled(event:Event):void{
			stageMask.visible = true;
		}
		
		/* Mask OFF */
		private function onAlertDisabled(event:Event):void{
			stageMask.visible = false;
		}
		
		/************************************************/
		/* Event handler for right-click items			*/
		/************************************************/
		
		private function onItemSelected(event:ContextMenuEvent){
			switch(event.target.caption){
				case "New Idea":
					newIdeaBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "Mark Root":
					rootBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "Move UP	(UP)":
					ideaUp();
					listNodes.dispatchEvent(new Event(Event.CHANGE));
					break;
				case "Move DOWN	(DOWN)":
					ideaDown();
					listNodes.dispatchEvent(new Event(Event.CHANGE));
					break;
				case "Load	(F7)":
					loadBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "Save	(F8)":
					saveBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "Cancel	(ESC)":
					cancelHandler();
					break;
				case "Delete	(Delete)":
					deleteBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "Help	(F1)":
					helpBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case "About Us	(F2)":
					aboutBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
			}
		}
		
		/* function for the event Keyboard.UP */
		private function ideaUp():Boolean{
			if(lastFocusObject is DiagramItem && rootObject != null){
				var keySet:Array = DiagramItem(lastFocusObject).hashMap.keySet();
				for(var i:int = 0; i<keySet.length; i++){
					if(searchParentNode(keySet[i], DiagramItem(lastFocusObject).name)){
						keySet[i].hashMap.moveUp(DiagramItem(lastFocusObject));
						return true;
					}
				}
			}
			return false;
		}
		
		/* function for the event Keyboard.DOWN */
		private function ideaDown():Boolean{
			if(lastFocusObject is DiagramItem && rootObject != null){
				var keySet:Array = DiagramItem(lastFocusObject).hashMap.keySet();
				for(var i:int = 0; i<keySet.length; i++){
					if(searchParentNode(keySet[i], DiagramItem(lastFocusObject).name)){
						keySet[i].hashMap.moveDown(DiagramItem(lastFocusObject));
						return true;
					}
				}
			}
			return false;
		}
		
		/* Event handler for the right-click 'Cancel' button */
		private function cancelHandler():void{
			if(isReactiveActivated){
				lineTimer.stop();
				isReactiveActivated = false;
				allObjs.remove(line.name);
				cldUI.removeChild(line);
				line = null;
			}
		}
		
		/************************************************/
		/* Event handler for stage						*/
		/************************************************/
		
		/* delete the focused object while the event had been triggered */
		private function onStageKeyDown(event:KeyboardEvent):void{
			switch(event.keyCode){
				case Keyboard.DELETE:
					deleteBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case Keyboard.ESCAPE:
					cancelHandler();
					break;
				case Keyboard.UP:
					ideaUp();
					listNodes.dispatchEvent(new Event(Event.CHANGE));
					break;
				case Keyboard.DOWN:
					ideaDown();
					listNodes.dispatchEvent(new Event(Event.CHANGE));
					break;
				case Keyboard.F1:
					helpBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case Keyboard.F2:
					aboutBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case Keyboard.F7:
					loadBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
				case Keyboard.F8:
					saveBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					break;
			}					
		}
		
		/*##### Line Connection ######*/
		
		/* Event handler for creating the connection for each two nodes */
		private function onReactiveClicked(event:Event):Boolean{
			if(!isReactiveActivated){
				isReactiveActivated = true;
								
				line = new Line(5, 0x000000, 1);
				line.buttonMode = true;
				
				line.startObj = event.target.name;
				startObj = DiagramItem(event.target);
				
				do{
					line.name = String(Math.random());	
					var ret:Boolean = allObjs.insert(line.name, line);
				}while(!ret);
						
				cldUI.addChild(line);
				cldUI.swapChildren(cldUI.getChildAt(cldUI.numChildren-1), cldUI.getChildAt(itemStartIndex));
				itemStartIndex++;
				
				fixOrder();
				
				lineTimer.start();				
			}else{
				lineTimer.stop();
				line.endObj = event.target.name;
				endObj = DiagramItem(event.target);
				line.repaint(startObj.x, startObj.y, endObj.x, endObj.y);
				
				line.addEventListener(MouseEvent.MOUSE_DOWN, onObjectClicked, false, 0, true);
				
				isReactiveActivated = false;
				
				if(!startObj.hashMap.insert(endObj, line)){
					allObjs.remove(line.name);
					cldUI.removeChild(line);
					alert.show("Redundant Line","Error!");
					itemStartIndex--;
					line = null;
					return false;
				}
				
				if(!endObj.hashMap.insert(startObj, line)){
					startObj.hashMap.remove(endObj);
					allObjs.remove(line.name);
					cldUI.removeChild(line);
					alert.show("Reduntant Line","Error!");
					itemStartIndex--;
					line = null;
					return false;
				}
				
				if(findClosedLoop(DiagramItem(event.target), null, DiagramItem(event.target), 0)){
					startObj.hashMap.remove(endObj);
					endObj.hashMap.remove(startObj);
					allObjs.remove(line.name);
					cldUI.removeChild(line);
					alert.show("Illegal Connection!","Error");
					itemStartIndex--;
					line = null;
					return false;
				}
				
				updateLines(DiagramItem(event.target));
				listNodes.dispatchEvent(new Event(Event.CHANGE));
				startObj = endObj = null;
				line = null;
			}			
			return true;
		}
		
		/* update line length */
		private function onLineTimer(event:TimerEvent):void{
			//var angle:Number = Math.atan2(cldUI.mouseX-startObj.y, cldUI.mouseY-startObj.x);
			//line.repaint(startObj.x, startObj.y, cldUI.mouseX-5*Math.cos(angle), cldUI.mouseY-5*Math.sin(angle));
			line.repaint(startObj.x, startObj.y, cldUI.mouseX, cldUI.mouseY);
		}
		
		/************************************************/
		/* HashMap traveling and searching algorithm	*/
		/************************************************/
		
		/* Displaying the tree node value as a text on TextArea */ 
		private function listAll(rootNode:DiagramItem, parentNode:String, level:int, prevCount:int = 0):void{
			var i:int = 0;
			var j:int = 0;
			var count:int = 1;
			
			var rootKeySet:Array = rootNode.hashMap.keySet();
			
			for(j = 0; j<level ;j++){
				listNodes.appendText("\t");
			}
			
			/*
			if(prevCount != 0){
				listNodes.appendText(prevCount+". ");
			}
			*/
			
			if(prevCount != 0){
				listNodes.appendText("- ");
			}
			
			/* regular expression for reducing or replacing the 'space'/'carriage return' character */
			listNodes.appendText(rootNode.text.replace(/[\r ]+/g, " ")+"\n");
			
			if(rootKeySet.length == 1 && rootKeySet[0].name == parentNode){
				return;
			}
			
			for(i = 0; i<rootKeySet.length ; i++){
				if(rootKeySet[i].name != parentNode){
					listAll(rootKeySet[i], rootNode.name, level+1, count);
					count++;
				}
			}
		}
		
		/* finding the illegal connection */
		/* Illegal when the value is great than 0 */ 
		private function findClosedLoop(rootNode:DiagramItem, parentNode:String ,startNode:DiagramItem, level:int):int{
			var rootKeySet:Array = rootNode.hashMap.keySet();
			var value:int = 0;
			
			if(level !=0 && rootNode === startNode) return 1;
			if(rootKeySet.length == 1 && rootKeySet[0].name == parentNode){
				return 0;
			}
			
			for(var i:int = 0; i<rootKeySet.length ; i++){
				if(rootKeySet[i].name != parentNode){
					if(value == 0){
						value += findClosedLoop(rootKeySet[i], rootNode.name, startNode, level+1);
					}else{
						return value;
					}
				}
			}
			
			return value;
		}
		
		/* find the parent node for moving UP or DOWN */
		private function searchParentNode(currNode:DiagramItem, fromNode:String):int{
			var currKeySet:Array = currNode.hashMap.keySet();
			var value:int = 0;
			
			if(currNode.name == rootObject.name) return 1;
			
			for(var i:int = 0; i<currKeySet.length ; i++){
				if(currKeySet[i].name != fromNode){
					if(value == 0){
						value += searchParentNode(currKeySet[i], currNode.name);
					}else{
						return value;
					}
				}
			}
			
			return value;
		}
		
		/************************************************/
		/* Event handler for diagramItem				*/
		/************************************************/
		
		/* set the focus object when follow event had benn triggered */
		private function onObjectClicked(event:MouseEvent):void{
			try{
				if(event.target is ItemShape){
					lastFocusObject = event.target.parent;
					stage.focus = DiagramItem(event.target.parent);
				}else if(event.target is Line){
					lastFocusObject = event.target;
					stage.focus = Line(event.target);
				}
			}catch(e:Error){
			}
		}
		
		/*######### Dragging ... #########*/
		
		/* Dragging events for diagramItem */
		private function onDragStart(event:MouseEvent):void{
			try{
				if(event.target is ItemShape){
					event.target.parent.startDrag(false, new Rectangle(event.target.parent.getWidth/2, event.target.parent.getHeight/2, A4L_WIDTH-event.target.parent.getWidth, A4L_HEIGHT-event.target.parent.getHeight));
					dragObj = DiagramItem(event.target.parent);
					diagramItemTimer.start();
				}
				
				/*
				// textfield object is dragable 
				
				else if(event.target is TextField){
					event.target.parent.startDrag(false, new Rectangle(event.target.parent.radius, event.target.parent.radius, A4L_WIDTH-2*event.target.parent.radius, A4L_HEIGHT-2*event.target.parent.radius));
					dragObj = event.target.parent;
					diagramItemTimer.start();					
				}
				*/
			}catch(e:Error){
			}
		}
		
		private function onDragStop(event:MouseEvent):void{
			try{
				if(dragObj){
					diagramItemTimer.stop();
					dragObj.stopDrag();
					dragObj = null;
				}				
			}catch(e:Error){
			}
		}
		
		/* updating lines while dragging */
		private function onDiagramItemTimer(event:TimerEvent):void{
			updateLines(dragObj);			
		}
		
		/* undocumented feature for change diagramItem size manually*/
		/*
		private function onWheel(event:MouseEvent):void{
			try{
				if(event.delta>0){
					if(event.target is TextField){
						event.target.parent.repaint(event.target.parent.thickness, event.target.parent.borderCol, event.target.parent.txtCol, event.target.parent.color, event.target.parent.radius+5);
						fixPos(event.target.parent, new Rectangle(event.target.parent.radius, event.target.parent.radius, (A4L_WIDTH-event.target.parent.radius), (A4L_HEIGHT-event.target.parent.radius)));
						updateLines(event.target.parent);
					}else{
						event.target.repaint(event.target.thickness, event.target.borderCol, event.target.txtCol, event.target.color, event.target.radius+5);
						fixPos(event.target as DiagramItem, new Rectangle(event.target.radius, event.target.radius, (A4L_WIDTH-event.target.radius), (A4L_HEIGHT-event.target.radius)));
						updateLines(event.target);
					}
				}else{
					if(event.target is TextField){
						event.target.parent.repaint(event.target.parent.thickness, event.target.parent.borderCol, event.target.parent.txtCol, event.target.parent.color, event.target.parent.radius-5);
						updateLines(event.target.parent);
					}else{
						event.target.repaint(event.target.thickness, event.target.borderCol, event.target.txtCol, event.target.color, event.target.radius-5);
						updateLines(event.target);
					}
				}
			}catch(e:Error){
			}
		}
		*/
		
		private function onTextFocusOut(event:Event):void{
			if(rootObject){
				listNodes.htmlText = "";
				listAll(DiagramItem(rootObject), null, 0, 0);
			}
		}
		
		/* Event handler for updating line's position, length and angle */
		private function onTextLineChanged(event:Event):void{
			updateLines(DiagramItem(event.target));
		}
		
		private function updateLines(src:DiagramItem):void{
			var keySet:Array = src.hashMap.keySet();
			var iter:Iterator = src.hashMap.iterator();
			var lineObj:Line;
			for(var i:int = 0; i<keySet.length ;i++){
				lineObj = iter.next();
				lineObj.repaint(src.x, src.y, keySet[i].x, keySet[i].y);
			}
		}
		
		/************************************************/
		/* Printing Job									*/
		/************************************************/
		
		/* Event handler for print button */
		private function printOnePerPage(event:MouseEvent):void {
            var pj:PrintJob = new PrintJob();
            var pagesToPrint:int = 0;
           	
			/* add cldUI as newUI's child */
           	var newUI:Sprite = new Sprite();
           	newUI.graphics.beginFill(0xffffff);
           	newUI.graphics.drawRect(0,0,846,595);
           	newUI.graphics.endFill();
			
			canvas.removeChild(cldUI);
           	canvas.addChild(newUI);
           	newUI.visible = false;			
			newUI.addChild(cldUI);
			
			cldUI.x += border;
			cldUI.y += border;
           	
            if(pj.start()) {
            	             
                if(pj.orientation == PrintJobOrientation.PORTRAIT) {
					/* rotate the newUI for portrait orientation */
					newUI.rotation = 90;
					//alert.show("The page was printed with portrait orientation!","Info");
                }else{
					//alert.show("The page was printed with landscape orientation!","Info");
				}
                             
                try {
                    pj.addPage(newUI);
                    pagesToPrint++;
                } catch(e:Error) {
                    
                }

                if(pagesToPrint > 0) {
                    pj.send();
                }
            }
            
			/* remove newUI and add cldUI to canvas */
			newUI.removeChild(cldUI);
			canvas.addChild(cldUI);
            canvas.removeChild(newUI);
			
			cldUI.x -= border;
			cldUI.y -= border;
        }
		
		/************************************************/
		/* Event Handler for each button				*/
		/************************************************/
		
		/* Event handler for 'New Idea' button */
		private function onIdeaBtnClicked(event:MouseEvent):void{
			var diagramItem:DiagramItem = new DiagramItem(5, 0x000000, 0x000000, 0xCCCCCC, 100, 100, "Rectangle");
			cldUI.addChild(diagramItem);
			
			if(rootObject == null){
				lastFocusObject = rootObject = diagramItem;
				rootBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
			}
			
			diagramItem.x = Math.random() * A4L_WIDTH;
			diagramItem.y = Math.random() * A4L_HEIGHT;
			
			fixPos(diagramItem, new Rectangle(diagramItem.getWidth/2, diagramItem.getHeight/2, A4L_WIDTH-diagramItem.getWidth/2, A4L_HEIGHT-diagramItem.getHeight/2));
			
			do{
				diagramItem.name = String(Math.random());
			}while(!allObjs.insert(diagramItem.name, diagramItem));			
			
			diagramItem.addEventListener(MouseEvent.MOUSE_DOWN, onDragStart, false, 0, true);
			diagramItem.addEventListener(MouseEvent.MOUSE_UP, onDragStop, false, 0, true);
			diagramItem.addEventListener(MouseEvent.MOUSE_UP, onObjectClicked, false, 0, true);
			/* undocumented feature */
			//diagramItem.addEventListener(MouseEvent.MOUSE_WHEEL, onWheel, false, 0, true);
			diagramItem.addEventListener("onTextFocusOut", onTextFocusOut, false, 0, true);
			diagramItem.addEventListener("onReactiveClicked", onReactiveClicked, false, 0, true);
			diagramItem.addEventListener("onTextLineChanged", onTextLineChanged, false, 0, true);
		}
		
		/* Event handler for 'Mark Root' button */
		private function onRootBtnClicked(event:MouseEvent):void{
			if(lastFocusObject is DiagramItem){
				if(rootObject){
					rootObject.repaint(rootObject.thickness, 0x000000, 0x000000, 0xCCCCCC, rootObject.getWidth, rootObject.getHeight);
					listNodes.htmlText = "";
					listAll(rootObject, null, 0, 0);
				}
				
				rootObject = DiagramItem(lastFocusObject);
				lastFocusObject.repaint(lastFocusObject.thickness, 0x000033, 0xffffff, 0x666666, lastFocusObject.getWidth, lastFocusObject.getHeight);
				listNodes.htmlText = "";
				listAll(rootObject, null, 0, 0);
			}else{
				alert.show("Please click a node first!","Error!");
			}
		}
		
		/* Event handler for 'Export...' button*/
		private function onSnapShotClicked(event:MouseEvent):void{
			var bmpdata:BitmapData = new BitmapData(A4L_WIDTH+10, A4L_HEIGHT+10);
			bmpdata.draw(canvas);
			
			var byteArr:ByteArray = PNGEncoder.encode(bmpdata);
			
			var fileName:String
			
			try{
				fileName = rootObject.text.replace(/[\r ]+/g, " ")+".png";
			}catch(e:Error){
				fileName = ".png";
			}
			
			var file:FileReference = new FileReference();
			file.save(byteArr, fileName);
		}
		
		/* Event handler for 'Open' button */
		private function onLoadBtnClicked(event:MouseEvent):void{			
			var imgFileExt:FileFilter = new FileFilter("Cluster Diagram", "*.cld");
			var allFileExt:FileFilter = new FileFilter("All Files", "*.*");
			
			loadFileRef = new FileReference();
			loadFileRef.addEventListener(Event.SELECT, onLoadFileSelected, false, 0, true);
			loadFileRef.addEventListener(Event.COMPLETE, onLoadCompleted, false, 0, true);
			
			loadFileRef.browse([imgFileExt, allFileExt]);
		}
		
		/* Event handler for file's Event.SELECT */
		private function onLoadFileSelected(event:Event):void{
			allObjs.clear();
			while(cldUI.numChildren){
				cldUI.removeChildAt(0);
				
				/* clear the common variables */
				lastFocusObject = rootObject = null;
				line = null;
			}
			
			event.target.load();
		}
		
		/*######## Load from file ########*/
		
		/* Event handler for file Event.COMPLETE */
		private function onLoadCompleted(event:Event):void{	
			var i:int;
			var j:int;
			var totalObjs:int;
			var numOfDiagramItem:int = 0;
			var length:Number;
			var ret:int;			
			var obj:Object;
			var diagramItem:DiagramItem;
			var lineObj:Line;
			var width:Number;
			var height:Number;
			var type:String;
			
			var byteArr:ByteArray = event.target.data;
			byteArr.position = 0;
			
			try{
				totalObjs = byteArr.readInt();

				for(i=0;i<totalObjs;i++){
					ret = byteArr.readInt();
					trace(ret);
					if(ret == 1){
						numOfDiagramItem++;
						
						width = byteArr.readDouble();
						height = byteArr.readDouble();
						length = byteArr.readInt();
						type = byteArr.readUTFBytes(length);
					
						diagramItem = new DiagramItem(5, 0x000000, 0x000000, 0xCCCCCC, width, height, type);
						diagramItem.addEventListener(MouseEvent.MOUSE_DOWN, onDragStart, false, 0, true);
						diagramItem.addEventListener(MouseEvent.MOUSE_UP, onDragStop, false, 0, true);
						diagramItem.addEventListener(MouseEvent.MOUSE_UP, onObjectClicked, false, 0, true);
						diagramItem.addEventListener("onTextFocusOut", onTextFocusOut, false, 0, true);
						diagramItem.addEventListener("onReactiveClicked", onReactiveClicked, false, 0, true);
						diagramItem.addEventListener("onTextLineChanged", onTextLineChanged, false, 0, true);
					
						length = byteArr.readInt();
						diagramItem.name = byteArr.readUTFBytes(length);
					
						diagramItem.x = byteArr.readInt();
						diagramItem.y = byteArr.readInt();
					
						length = byteArr.readInt();
						diagramItem.text = byteArr.readUTFBytes(length);
					
						cldUI.addChild(diagramItem);
						allObjs.insert(diagramItem.name, diagramItem);					
					}else if(ret == 2){
						lineObj = new Line(4, 0x000000, 1);
						lineObj.buttonMode = true;
						lineObj.addEventListener(MouseEvent.MOUSE_DOWN, onObjectClicked, false, 0, true);
						
						// name
						length = byteArr.readInt();
						lineObj.name = byteArr.readUTFBytes(length);
						
						cldUI.addChild(lineObj);
						allObjs.insert(lineObj.name, lineObj);

						// records
						length = byteArr.readInt();
						lineObj.startObj = byteArr.readUTFBytes(length);
						length = byteArr.readInt();
						lineObj.endObj = byteArr.readUTFBytes(length);
					}
				}
			
				numOfDiagramItem = byteArr.readInt();
				var keyLength:int;
			
				for(i=0;i<cldUI.numChildren;i++){
					obj = cldUI.getChildAt(i);
					if(obj is DiagramItem){
						keyLength = byteArr.readInt();
										
						for(j=0;j<keyLength;j++){
							length = byteArr.readInt();
							diagramItem = allObjs.find(byteArr.readUTFBytes(length));
						
							length = byteArr.readInt();
							lineObj = allObjs.find(byteArr.readUTFBytes(length));
						
							obj.hashMap.insert(diagramItem, lineObj);						
						}
						updateLines(DiagramItem(obj));						
					}
				}
		
				length = byteArr.readInt();
				if(length){
					lastFocusObject = rootObject = allObjs.find(byteArr.readUTFBytes(length));
					rootBtn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					listNodes.dispatchEvent(new Event(Event.CHANGE));
				}else{
					lastFocusObject = rootObject = null;
				}
				
			}catch(e:Error){
			}
			
			/* clean the fileReference */
			loadFileRef.removeEventListener(Event.SELECT, onLoadFileSelected);
			loadFileRef.removeEventListener(Event.COMPLETE, onLoadCompleted);
			loadFileRef = null;
		}
		
		/*######## Save as file ##########*/
		
		/* Event handler for 'Save' button */
		private function onSaveBtnClicked(event:MouseEvent):void{			
			var i:int;
			var j:int;
			var obj:Object;
			
			var objName:String;
			var keySet:Array;
			var byteArr:ByteArray = new ByteArray();
			
			var numOfDiagramItem:int = 0;
			
			byteArr.writeInt(cldUI.numChildren);
			
			// save basic information for diagramItem and line 
			for(i=0;i<cldUI.numChildren;i++){
				obj = cldUI.getChildAt(i);
				
				if(obj is DiagramItem){
					numOfDiagramItem++;
					
					// type id
					byteArr.writeInt(1);
					
					// width and height
					byteArr.writeDouble(obj.getWidth);
					byteArr.writeDouble(obj.getHeight);
					
					// type
					byteArr.writeInt(obj.type.length);
					byteArr.writeUTFBytes(obj.type);
					
					// name
					byteArr.writeInt(obj.name.length);
					byteArr.writeUTFBytes(obj.name);
					
					// pos
					byteArr.writeInt(obj.x);
					byteArr.writeInt(obj.y);
					
					// text
					byteArr.writeInt(obj.text.length);
					byteArr.writeUTFBytes(obj.text);
					
				}else if(obj is Line){
					// type id
					byteArr.writeInt(2);
					
					// name
					byteArr.writeInt(obj.name.length);
					byteArr.writeUTFBytes(obj.name);
					
					// records
					byteArr.writeInt(obj.startObj.length);
					byteArr.writeUTFBytes(obj.startObj);
					byteArr.writeInt(obj.endObj.length);
					byteArr.writeUTFBytes(obj.endObj);
				}
			}
			
			// save each diagramItem's hashMap;
			
			byteArr.writeInt(numOfDiagramItem);
			
			for(i=0;i<cldUI.numChildren;i++){
				obj = cldUI.getChildAt(i);
				if(obj is DiagramItem){
					keySet = obj.hashMap.keySet();
					byteArr.writeInt(keySet.length);
					
					for(j=0;j<keySet.length;j++){
						// key's string
						byteArr.writeInt(keySet[j].name.length);
						byteArr.writeUTFBytes(keySet[j].name);
						
						// value's string
						objName = obj.hashMap.find(keySet[j]).name;
						byteArr.writeInt(objName.length);
						byteArr.writeUTFBytes(objName);
					}
				}
			}
			
			// rootObject
			if(rootObject){
				byteArr.writeInt(rootObject.name.length);
				byteArr.writeUTFBytes(rootObject.name);
			}else{
				byteArr.writeInt(0);
			}
			
			
			//stageMask.visible = true;
			var fileName:String;
			try{
				fileName = rootObject.text.replace(/[\r ]+/g, " ")+".cld";
			}catch(e:Error){
				fileName = "cluster.cld";
			}
			
			
			var saveFileRef:FileReference = new FileReference();
			saveFileRef.save(byteArr, fileName);
		}
		
		/* Event handler for 'delete' button */
		private function onDelBtnClicked(event:MouseEvent):void{
			if(!(stage.focus is Button)){
				lastFocusObject = stage.focus;
			}
			
			if(lastFocusObject is Sprite){
				/* if the focused object is Line, just remove the record of the connected nodes */
				if(lastFocusObject is Line){
					allObjs.find(lastFocusObject.startObj).hashMap.remove(allObjs.find(lastFocusObject.endObj));
					allObjs.find(lastFocusObject.endObj).hashMap.remove(allObjs.find(lastFocusObject.startObj));
					itemStartIndex--;
				}else if(lastFocusObject is DiagramItem){					
					if(lastFocusObject == rootObject){
						rootObject = null;
						listNodes.htmlText = "";
					}
					
					var keySet:Array = lastFocusObject.hashMap.keySet();
					var iter:Iterator = lastFocusObject.hashMap.iterator();
					var lineObj:Line;
					for(var i:int = 0; i<keySet.length;i++){
						/* remove the target object record from the connected node */
						keySet[i].hashMap.remove(lastFocusObject);
							
						/* remove the connection line from main hashMap and cldUI */
						lineObj = iter.next();
						destory(lineObj);
						
						cldUI.removeChild(lineObj);
						allObjs.remove(lineObj);
						itemStartIndex--;
					}
				}
					
				/* remove the target from main hashMap and cldUI */
				destory(Sprite(lastFocusObject));
				allObjs.remove(lastFocusObject.name);
				cldUI.removeChild(Sprite(lastFocusObject));
				
				/* if the object is DiagramItem, remove the connected noedes' record and lines */				
				if(rootObject){
					/* update list information */
					listNodes.htmlText = "";
					listAll(rootObject, null, 0, 0);
				}
				
				lastFocusObject = null;
			}
		}
		
		/* Event handler for 'Export Outline' button */
		private function onClipBtnClicked(event:MouseEvent):void{
			listNodes.dispatchEvent(new Event(Event.CHANGE));
			
			Clipboard.generalClipboard.clear();
            Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, listNodes.text);
		}
		
		/*  Event handler for 'Help' button */
		private function onHelpClicked(event:MouseEvent):void{
			if(helpWindow.visible){
				helpWindow.visible = false;
			}else{
				helpWindow.visible = true;
			}
		}
		
		/*  Event handler for 'About' button */
		private function onAboutClicked(event:MouseEvent):void{
			if(aboutWindow.visible){
				aboutWindow.visible = false;
			}else{
				aboutWindow.visible = true;
			}
		}
		
		/************************************************/
		/* Error correction								*/
		/************************************************/
		
		/* fix item's position */
		private function fixPos(obj:DiagramItem, range:Rectangle):void{
        	if(obj.x < range.x){
        		obj.x = range.x;
        	}
        	
        	if(obj.y < range.y){
        		obj.y = range.y;
        	}
        	
        	if(obj.x > range.width){
        		obj.x = range.width;
        	}
        	
        	if(obj.y > range.height){
        		obj.y = range.height;
        	}
        }
		
		/** in Cluster 1.2, put all Line objects behind the DiagramItem. **/ 
		private function fixOrder():void{
			var lastLineDepth:int = 0;
			var firstItemDepth:int = 0;
			var i:int;
			
			while(1){
				for(i=0;i<cldUI.numChildren;i++){
					if(cldUI.getChildAt(i) is Line) lastLineDepth = i;
				}
				
				for(i=cldUI.numChildren-1; i>=0;i--){
					if(cldUI.getChildAt(i) is DiagramItem) firstItemDepth = i;
				}
				
				if(lastLineDepth > firstItemDepth){
					cldUI.swapChildren(cldUI.getChildAt(lastLineDepth), cldUI.getChildAt(firstItemDepth));
				}else{
					break;
				}
			}
		}
		
		
		/************************************************/
		/* Garbage Collection							*/
		/************************************************/
		
		/* Destory the unused object manually */
		private function destory(obj:Sprite):void{
			if(obj is DiagramItem){
				var item:DiagramItem = DiagramItem(obj);
				item.removeEventListener(MouseEvent.MOUSE_DOWN, onDragStart);
				item.removeEventListener(MouseEvent.MOUSE_UP, onDragStop);
				item.removeEventListener(MouseEvent.MOUSE_UP, onObjectClicked);
				item.removeEventListener("onTextFocusOut", onTextFocusOut);
				item.removeEventListener("onReactiveClicked", onReactiveClicked);
				item.removeEventListener("onTextLineChanged", onTextLineChanged);
				
				item.destory();
			}else{
				Line(obj).removeEventListener(MouseEvent.MOUSE_DOWN, onObjectClicked);
			}
		}
	}
}